---
title: Olá, mundo!
kind: article
created_at: 2019-03-24 19:00
---

Em discussões do grupo [Curitiba Livre](http://curitibalivre.org.br/), nós
percebemos que não havia no Brasil entidades que atuem como "guarda chuva
institucional" para que projetos nacionais, normalmente informais, possam receber
doações, contratar serviços, celebrar contratos, manter ativos, e realizar
outras ações que requeiram uma personalidade jurídica. Nossos próprios projetos
locais, os eventos que organizamos em Curitiba, sofriam deste problema!

Iniciamos as discussões para a criação de uma entidade assim, pesquisamos a
atuação de entidades estrangeiras semelhantes, e estávamos discutindo
internamente" com calma a criação da nossa entidade.  Foi quando o nosso grupo
teve aceita uma proposta para a realização da [DebConf
19](https://debconf19.debconf.org/), a conferência internacional do projeto
Debian, em 2019 em Curitiba. A DebConf é um projeto grande, e que precisaria de
uma estrutura jurídica no Brasil, principalmente para celebrar contratos,
receber doações nacionais, e realizar despesas localmente.

Desta forma, em meados do ano de 2018, nós formalizamos a criação do ICTL,
Instituto para Conservação de Tecnologias Livres. O nome é inspirado na
[Software Freedom Conservancy](https://sfconservancy.org/), entidade com sede
nos Estados Unidos que realiza o mesmo trabalho para projetos internacionais.

No momento, estamos bastante ocupados com a organização da DebConf 19, mas
temos planos ambiciosos para o futuro:

* Vamos publicar em detalhes os critérios para aceitar novos projetos membros
  do ICTL, mas podemos adiantar que projetos membros precisam ser projetos
  comunitários de software (ou hardware) livre, que funcionem de forma
  transparente e não centralizada, e quem tenham um código de conduta razoável.
* O próprio ICTL vai prestar contas publicamente de toda a sua movimentação
  financeira, o que inclui as contas dos projetos membros.
* O ICTL vai cobrar uma taxa sob todo recurso recebido pelos projetos membros
  para ajudar com a manutenção da entidade, e a realização de atividades do
  próprio ICTL.
* Vamos também ter um programa de colaboradores para que pessoas que acreditam
  no ICTL possam apoiar financeiramente a entidade, mensal ou anualmente.
