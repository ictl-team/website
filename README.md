Este repositório contém os fontes do site do ICTL. Ele usa
[nanoc](https://nanoc.ws/), um gerador de sites estáticos.

## Como testar o site localmente

Pacotes necessários: instale os pacotes listados no
[.gitlab-ci.yml](.gitlab-ci.yml).

Num terminal, entre no diretório raiz do site, e execute:

```
nanoc live
```

Acesse sua versão local do site em <http://localhost:3000/>.

Você pode usar um outro terminal pra editar o arquivos, ou usar um editor
gráfico, tanto faz.

Da primeira vez, a compilação pode levar alguns segundos. A partir daí, cada
recompilação deve ser bem rápida, exceto quando você altera um arquivo que faz
com que o site todo seja modificado (ex. `layouts/default.html`).

Se você fizer uma alteração, der um reload, e a página em questão não for
atualizada, verifique o terminal pra ver se não tem uma mensagem de erro.
