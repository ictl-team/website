---
title: Projetos
---

# Projetos membros do ICTL

## Debian Brasil

![Logo do Debian Brasil](/projetos/debianbrasil.svg)

O [Debian Brasil](https://debianbrasil.org.br/) é um projeto formado por
usuários(as) do sistema operacional Debian, e por pessoas que colaboram para o
Projeto Debian como tradutores(as), documentadores(as), designers,
organizadores de eventos, mantenedores(as) e desenvolvedores(as) de pacotes de
software. Com apoio do ICTL, a comunidade Debian realiza diversos eventos pelo
Brasil divulgando e atraindo novas pessoas para colaborar com o projeto Debian
e com o software livre em geral.

## FOSForums

![Logo da FOSForums](/projetos/fosforums.png)

A [FOSForums](https://www.fosforums.org/) (Free and Open Source Forums) é uma
conferência focada em software e hardware livres e abertos e as suas
comunidades.

## DebConf 19

![Logo da DebConf 19](/projetos/debconf19.png)

A [19ª edição da DebConf](https://debconf19.debconf.org/), a conferência anual
da comunidade Debian, acontece em Curitiba entre 14 e 28 de Julho de 2019. Além
de uma programação completa com palestras técnicas, sociais e políticas, a
DebConf cria uma oportunidade para desenvolvedores(as), contribuidores(as) e
outras pessoas interessadas se conhecerem presencialmente e trabalharem juntas
de maneira mais próxima

## Linux Developer Conference Brazil

![Logo da Linuxdev-br](/projetos/linuxdev-br.png)

A [Linux Developer Conference Brazil](https://linuxdev-br.net/) é uma
conferência internacional focado na discussão de projetos de infra-estrutura do
mundo do Software Livre como Linux (o kernel), systemd, containers, wayland,
Gstreamer, gcc, llvm, MESA, gdb, bootloaders, qemu, kvm, ChromeOS, tracing,
segurança, e outros componentes de baixo nível.

## FLOSS at USP (FLUSP)

![Logo FLUSP](/projetos/flusp.png)

O [FLUSP](https://flusp.ime.usp.br/) é um grupo de extensão da Universidade de
São Paulo (USP) formado por alunos de graduação e pós-graduação que visa
contribuir com projetos de software livre (Free/Libre and Open Source Software
- FLOSS). Dentre outros projetos, atualmente eles contribuem com o Kernel
Linux, GCC, Kworkflow e Git. O grupo realiza encontros semanais no Centro de
Competência de Software Livre ([CCSL](http://ccsl.ime.usp.br/)) do Instituto de
Matemática e Estatística ([IME-USP](https://www.ime.usp.br/en)).
