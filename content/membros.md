---
title: Membros
---

# Membros do ICTL

* Antonio Carlos da Conceição Marques (Conselheiro Fiscal)
* Antonio Soares de Azevedo Terceiro  (Tesoureiro)
* Cleber de Oliveira Campos Barco Ianes (Conselheiro Fiscal)
* Daniel Lenharo de Souza  (Presidente)
* Giovani Augusto Ferreira
* Leonardo Rodrigues Pereira (Secretario)
* Lucas Kanashiro Duarte
* Paulo Henrique de Lima Santana (Conselheiro Fiscal)
* Thiago Faria Mendonça
* Valéssio Soares de Brito
