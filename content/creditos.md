---
title: Créditos
---

# Créditos do site


Exceto quanto explicitamente mencionado ao contrário, o conteúdo deste site
está licenciado sob a licença CreativeCommons
[Atribuição-CompartilhaIgual 4.0 Internacional (CC BY-SA 4.0)](https://creativecommons.org/licenses/by-sa/4.0).
O site é feito com [Nanoc](https://nanoc.ws/), e [o código fonte está disponível](https://salsa.debian.org/ictl-team/website).

O ícone para Feed RSS
(![Feed RSS](/img/rss-square-solid.svg))
foi obtido no [site da FontAwesome](https://fontawesome.com/icons/rss-square?style=solid)
e está licenciado sob os terms da licença
[Creative Commons Attribution 4.0 International](https://creativecommons.org/licenses/by/4.0).
Ele foi modificado para que as margens do desenho coincidissem com os limites
do ícone e reduzido para 24x24 pixels. Sua cor foi trocada do preto para o
laranja, e ele foi otimizado com [scour].

As bandeiras utilizadas no menu de idiomas foram obtidas do projeto
[flag-icon-css](http://flag-icon-css.lip.is/), e estão licenciadas sob a
licença MIT (Expat). A bandeira do Brasil foi simplificada removendo os
detalhes de texto e as estrelas para reduzir o tamanho do arquivo, e todas elas
foram otimizadas com [scour].

[scour]: https://github.com/codedread/scour
