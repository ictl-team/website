# Apoie o ICTL

Sexta-feira, 25 de Agosto de 2023.

Olá! Nós estamos organizando nosso programa de associados contribuidores,
incluindo valores de contribuição e benefícios, e em breve você vai poder
contribuir com o ICTL. Para receber um aviso quando isso estiver pronto,
preencha o seu email no formulário abaixo.

<form method="POST" action="https://mailing.ictl.org.br/">
<input type="text" name="email" size="50" required placeholder="seu email"/>
<input type="hidden" name="name">
<input type="submit" value="Me avise">
</form>
