---
title: Home
---

# Institute for the Conservation of Free Technologies

The ICTL -- Institute for the Conservation of Free Technologies [^1] --
is a non-profit association which aims to promote the dissemination,
development and defense of Free Software. In addition to developing actions for
the advancement of Free Software in general, the Institute also provides an
institutional structure for Free Software projects, so that they can receive
donations, incur expenses, and maintain resources.

ICTL is incorporated in the city of Curitiba, Brazil, and its bylaws include
the following objectives:

* Fostering the use and development of Free Software and Hardware;
* Supportting and Publicizing Free Software and Hardware projects;
* Supporting Digital Inclusion projects that use Free Software;
* Incentivizing the adoption of free software by governments at the city, state
  and federal level;
* Supporting other national and international institutions that also have
  similar free software-related objectives.
* Producing free software-related events, courses, and campaigns and other
  activities, of technical, philosophical, social or educational nature;
* Promoting courses, seminaras, conferences and other events towards training,
  information, and diffusion of knowledge about Free Software.

## More information (in Portuguese)

As a Brazilian institution and with the Brazilian territory our main focus of
activity, most of our content is only available in Brazilian Portuguese. In any
case, and since nowadays automated translation can get you a long way, we
reference it here in case you need more information about us:

* [Projects](/projetos/)
* [Members](/membros/)
* [Bylaws](/estatuto/)
* [Full credits for this website](/creditos/)

## Contact

For general inquiries, write to
[contato@ictl.org.br](mailto:contato@ictl.org.br)

For financial matters, write to
[financeiro@ictl.org.br](mailto:financeiro@ictl.org.br)

---
[^1]:
    The acronym comes from our original name in Portuguese: Instituto para
    Conservação de Tecnologias Livres.  (Yes, we are big fans of the [Software
    Freedom Conservancy](https://sfconservancy.org/))
