# Estatuto - Instituto para Conservação de Tecnologias Livres

Abaixo está reproduzido o estatuto social do ICTL, registrato em cartório na
cidade de Curitiba/PR.

Download:

* [ODT (LibreOffice)](ictl-estatuto-social.odt)
* [PDF](ictl-estatuto-social.pdf)
* [PDF (scan)](ictl-estatuto-social-scan.pdf)

------------------------------------------------------------------------

## CAPÍTULO I - DA DENOMINAÇÃO, SEDE E FINALIDADES

Art. 1º -- O **INSTITUTO PARA CONSERVAÇÃO DE TECNOLOGIAS LIVRES**
fundado em doze de abril de dois mil e dezoito (12/04/2018) é uma
pessoa jurídica de direito privado, sem fins econômicos, cujo prazo de
duração é indeterminado. Possui sede e foro na cidade de Curitiba,
estado do Paraná, e tem como área de atuação todo o território nacional.

§ Único -- O **INSTITUTO PARA CONSERVAÇÃO DE TECNOLOGIAS LIVRES**
adotará a sigla **ICTL**, pela qual será doravante referida.

Art. 2º -- O **ICTL** tem sua sede na Avenida Comendador Franco,
número 8115, Bloco A5 apartamento 14, bairro Uberaba, CEP 81560-001,
Curitiba – PR.

Art. 3º - O **ICTL** tem por finalidades:

a)  Incentivo ao uso e desenvolvimento de Software Livre e Hardware
    Livre;
b)  Apoio e divulgação de projetos de Software Livre e Hardware Livre ;
c)  Apoio a projetos de inclusão digital realizados com Software Livre;
d)  Incentivo a adoção de Software Livre pelos governos municipais,
    estaduais e federal;
e)  Apoio a outras entidades nacionais e internacionais que tenham
    finalidades comuns relacionadas a Software Livre.
f)  Promover cursos, seminários, congressos e outros eventos de capacitação,
    informação e difusão de conhecimento sobre Software Livre.

§ 1º -- O **ICTL** não distribui entre seus associados, conselheiros,
diretores, empregados ou doadores eventuais excedentes operacionais,
brutos ou líquidos, dividendos, bonificações, participações ou parcelas
do seu patrimônio, auferidos mediante o exercício de suas atividades, e
os aplica integralmente na consecução do seu objetivo social.

§ 2º -- O **ICTL** obterá os recursos para sua manutenção e para o
cumprimento de sua finalidade social através de campanhas públicas para
angariar doações de terceiros interessados em colaborar com a entidade.

§ 3º -- Não obstante a sua finalidade primordialmente beneficente, de
que não deverá afastar-se, o **ICTL** poderá cobrar taxa pelos serviços
que vier a prestar a pessoas físicas ou jurídicas em condições de
satisfazer os pagamentos.

Art. 4º - No desenvolvimento de suas atividades, o **ICTL** observará os
princípios da legalidade, impessoalidade, moralidade, publicidade,
economicidade e da eficiência e não fará qualquer discriminação de raça,
cor, gênero, orientação sexual, religiosa e política.

§ Único -- Para cumprir seu propósito a entidade atuará por meio da
execução direta de projetos, programas ou planos de ações, da doação de
recursos físicos, humanos e financeiros, ou prestação de serviços
intermediários de apoio a outras organizações sem fins econômicos e a
órgãos do setor público que atuam em áreas afins.

Art. 5º - O **ICTL** terá um Regimento Interno que, aprovado pela
Assembleia Geral, disciplinará o seu funcionamento.

Art. 6º - A fim de cumprir suas finalidades, a instituição se organizará
em tantas unidades de prestação de serviços, quantas se fizerem
necessárias, as quais se regerão pelas disposições estatutárias.

§ 1º -- Cada projeto local terá uma coordenação com no mínimo duas
pessoas, sendo que o coordenador será indicado pela direção;

§ 2º -- As coordenações citadas neste artigo servem para organizar,
agir, fazer e zelar pelos projetos em desenvolvimento e promover o
**ICTL** na localidade em que atuam, sendo que nunca deverão
desrespeitar as orientações estatutárias desta entidade mantenedora.

## CAPÍTULO II --- DOS ASSOCIADOS

Art. 7º - O **ICTL** compreende apenas associados efetivos.

§ Único -- Associados efetivos são todos os participantes da Assembleia
de fundação do **ICTL** e aqueles que são convidados a ingressar no
quadro social por no mínimo 3/4 (três quartos) dos associados efetivos
atuais e referendados pela Assembleia Geral, exercendo direito de votar
e ser votado na próxima Assembleia Geral.

Art. 8º - O **ICTL** reúne um número ilimitado de associados admitidos e
referendado pela Assembleia Geral.

§ Único -- Somente pessoas físicas são admitidas no quadro de associados
do **ICTL**.

Art. 9º - O **ICTL** terá contribuintes que contribuirão financeiramente
de forma mensal ou anual, conforme definições da Diretoria.

§ Único -- Estes contribuintes, não integram o quadro de associados do
**ICTL**.

Art. 10º -- São direitos dos associados efetivos:

a)  Votar e ser votado para os cargos eletivos;
b)  Participar das Assembleias Gerais com direito a voz e voto;
c)  Participar de todas as atividades a que a entidade esteja direta ou
    indiretamente ligada;
d)  Participar nas reuniões abertas da Diretoria com direito de voz;
e)  Convocar a Assembleia Geral.

Art. 11º - São deveres dos associados efetivos:

a)  Cumprir as disposições estatutárias e regimentais;
b)  Contribuir financeiramente com a entidade, pagando o valor mínimo
    fixado pela Assembleia Geral;
c)  Colaborar com a Diretoria na consecução dos trabalhos e objetos do
    **ICTL**;
d)  Comparecer regularmente as Assembleias Gerais e a outros atos da
    entidade;
e)  Manter seu cadastro de associado sempre atualizado, comunicando
    imediatamente o **ICTL** quaisquer alterações.

§ Único -- Os associados efetivos do **ICTL** poderão ser excluídos pela
Diretoria, cabendo sempre da decisão, recurso a Assembleia Geral e
respeitado o direito de defesa:

a)  Pelo não comparecimento, sem motivo justificado, por dois anos
    consecutivos, às Assembleias Gerais;
b)  Quando o associado praticar atos contrários ao seu dever para com a
    entidade e/ou encontrar-se inadimplente;
c)  Quando for reconhecida a existência de motivos graves ou o associado
    se afastar dos objetivos da entidade.

Art. 12º -- Os associados da entidade não respondem, nem mesmo
subsidiariamente, pelas obrigações e encargos sociais da instituição.

Art. 13º - A demissão do associado será feita:

a)  Por requerimento deste;
b)  Por dissolução da pessoa jurídica;
c)  Por morte do associado;
d)  Por incapacidade civil não suprida;
e)  Por justa causa.

Art. 14º - Constitui justa causa punível com a exclusão do associado
que:

a)  a)  Desacatar ou desrespeitar as decisões da entidade;
    b)  Ofender ou atingir a própria entidade, seus dirigentes,
        associados, ou descumprir o presente Estatuto;
    c)  Manifestar-se, por palavras ou gestos públicos ou mesmo
        reservadamente, contra os princípios da entidade;
    d)  Praticar atos que prejudiquem, direta ou indiretamente, os
        interesse e objetivos do **ICTL**, ou que, atentem ou
        comprometam seus princípios e objetivos;
    e)  Não pagar as contribuições instituídas.

§ 1º -- A exclusão poderá dar-se por iniciativa da diretoria ou por
requerimento de qualquer associado, e dessa decisão poderá o excluído
apresentar recurso à Assembleia Geral.

§ 2º -- A retirada voluntária de qualquer dos associados poderá ocorrer
desde que o interessado esteja quite com suas obrigações sociais e
estatutárias, bem como apresente aviso prévio de 15 (quinze) dias.

Art. 15º - Nenhum associado poderá ser impedido de exercer direito ou
função que lhe tenha sido legitimamente conferido, a não ser nos casos e
pela forma previstos na lei ou no estatuto.

## CAPÍTULO III --- DA ADMINISTRAÇÃO

Art. 16º - São órgãos da administração:

a)  Assembleia Geral
b)  Diretoria
c)  Conselho Fiscal

§ 1º -- A entidade poderá remunerar seus dirigentes pelos serviços
prestados, conforme aprovação da Assembleia Geral.

§ 2º -- A entidade poderá remunerar profissionais que lhe prestarem
serviços específicos, respeitados, nestes casos, os valores praticados
pelo mercado na região onde exerce suas atividades e o grau de
comprometimento com os projetos desenvolvidos, sendo para tanto
necessário descaracterizar, a critério da direção, o trabalho voluntário
para haver remuneração.

Art. 17º - A Assembleia Geral, órgão soberano do **ICTL**, se
constituirá de todos os associados efetivos, quites com suas obrigações
estatutárias, reunindo-se ordinariamente no primeiro semestre de cada
ano e, extraordinariamente por convocação da Diretoria, ou por 1/5 (um
quinto) dos associados.

§ 1º -- A Assembleia será convocada com antecedência mínimo de 15 dias
corridos por meio de edital de convocação remetida para o e-mail
indicado no cadastro do associado, e disponibilizado no website do
**ICTL**.

§ 2º -- A instalação da Assembleia Geral depende de um quorum mínimo de
2/3 (dois terços) dos associados efetivos em primeira convocação, e meia
hora depois em segunda e última convocação com qualquer quorum.

§ 3º -- A Assembleia Geral deve ser transmitida pela internet para
permitir a participação remota dos associados que poderão ouvir, opinar
e votar.

Art. 18º - A entidade adotará práticas de gestão administrativa,
necessárias e suficientes, a coibir a obtenção, de forma individual ou
coletiva, de benefícios e vantagens pessoais, em decorrência da
participação nos processos decisórios.

Art. 19º - Os Associados garantem que manterão seu comprometimento ético
na condução do **ICTL**, obrigando-se mediante a aceitação do presente
Estatuto a agir exclusivamente em plena consonância com os ditames
nacionais e estrangeiros relativos as medidas anticorrupção ("legislação
anticorrupção aplicável"), em especial, mas não se limitando a Lei
12.846/2013 e Lei Anticorrupção dos Estados Unidos da América (Foreign
Corrupt Practices Act -- FCPA).

Art. 20º - Compete à Assembleia Geral:

a)  Eleger a Diretoria e o Conselho Fiscal;
b)  Destituir a Diretoria e o Conselho Fiscal, com a concordância de 2/3
    (dois terços) dos associados, presentes em Assembleia convocada
    especialmente para este fim, não podendo tal deliberação ocorrer sem
    a maioria absoluta dos associados em primeira convocação ou 1/3 (um
    terço) nas convocações seguintes;
c)  Alterar o presente estatuto, com a concordância de 2/3 (dois terços)
    dos associados, presentes em Assembleia convocada especialmente para
    este fim, não podendo tal deliberação ocorrer sem a maioria absoluta
    dos associados em primeira convocação ou 1/3 (um terço) nas
    convocações seguintes;
d)  Aprovar a prestação de contas;
e)  Aprovar a proposta de programação anual e orçamento do **ICTL**,
    apresentada pela Diretoria;
f)  Referendar a admissão, demissão e exclusão de associados decidida
    pela Diretoria;
g)  Decidir sobre a conveniência de alienar, transigir, hipotecar ou
    permutar bens patrimoniais;
h)  Fixar anualmente a contribuição dos associados;
i)  Aprovar o regimento interno, proposto pela Diretoria.

Art. 21º - Compõem a Diretoria, eleita pela Assembleia Geral:

a)  Presidente
b)  Secretário
c)  Tesoureiro

§ 1º -- Os diretores assumirão na ordem de eleição, com mandato de 02
(dois) anos, permitido apenas uma reeleição para o mesmo cargo.

§ 2º -- A diretoria criará, comporá e dissolverá cargos, departamentos e
comissões tantos quanto forem necessários ao andamento das atividades do
**ICTL**.

§ 3º -- A Diretoria se reunirá no mínimo uma vez por trimestre.

Art. 22º - Compete a Diretoria:

a)  Elaborar e submeter à Assembleia Geral a proposta orçamentária e de
    programação do **ICTL**;
b)  Elaborar o regimento interno do **ICTL** e submetê-lo a Assembleia
    Geral para aprovação;
c)  Elaborar para conhecimento da Assembleia Geral, um relatório anual
    de atividades desenvolvidas pela entidade;
d)  Decidir sobre admissão, demissão e exclusão de associados, com a
    posterior referendum da Assembleia Geral;
e)  Gerir o patrimônio do **ICTL**;
f)  Contratar e dispensar empregados;
g)  Convocar a Assembleia Geral ordinária e extraordinária;
h)  Reunir-se com instituições públicas e privadas;
i)  Tomar decisões de cunho político institucional sempre por maioria
    absoluta;
j)  Coordenar as atividades dos programas em desenvolvimento;
k)  Reunir-se ordinariamente uma vez por mês, ou extraordinariamente por
    convocação do Presidente.

§ Único -- Em caso de vacância no cargo de Presidente, Tesoureiro ou
Secretário, a Diretoria convoca o vice e ele entre seus membros o
substituto para o cargo.

Art. 23º - O Conselho Fiscal é composto por 03 (três) titulares, que
assumirão na ordem de eleição, com mandato coincidente com o mandato da
Diretoria, permitida somente uma reeleição.

Art. 24º - Compete ao Conselho Fiscal:

a)  Examinar os livros de escrituração da entidade;
b)  Examinar os balanços e relatórios de desempenho financeiro e
    contábil e sobre as operações patrimoniais realizadas, emitindo
    pareceres sobre os mesmos para organismos superiores da entidade;
c)  Requisitar ao Tesoureiro, a qualquer tempo, documentação
    comprobatória das operações econômico-financeiras realizadas pela
    entidade;
d)  Acompanhar o trabalho de eventuais auditores externos independentes;
e)  Convocar extraordinariamente a Assembleia Geral.

§ Único -- O Conselho Fiscal se reunirá ordinariamente a cada 03 (três)
meses e extraordinariamente, sempre que necessário.

Art. 25º - Compete ao Presidente:

a)  Representar o **ICTL** ativa, passiva, judicial e
    extrajudicialmente, além de tratar das relações exteriores, buscando
    recursos humanos, financeiros e parcerias;
b)  Cumprir e fazer cumprir este estatuto e o regimento interno;
c)  Convocar e presidir a Assembleia Geral;
d)  Convocar e presidir as reuniões da Diretoria;
e)  Supervisionar e administrar os programas e projetos desenvolvidos;
f)  Assinar convênios, contratos, documentos financeiros, movimentar
    contas bancárias e emitir cheques, sempre em conjunto com o
    Tesoureiro.

§ Único -- Caso o Presidente tiver qualquer tipo de impedimento para
efetuar as transações bancárias em nome da entidade, assumirá esta
responsabilidade o Secretário.

Art. 26º - Compete ao Secretário:

a)  Substituir o Presidente em suas faltas, impedimentos ou delegações
    de poderes, e assumir o mandato de Presidente, em caso de vacância;
b)  Auxiliar o Presidente em seus encargos e supervisionar as atividades
    do **ICTL**.
c)  Fiscalizar o patrimônio do **ICTL** e zelar por ele.
d)  Superintender os serviços da secretaria.
e)  Ter em seu encargo o expediente geral do **ICTL**.
f)  Organizar e ter sob sua guarda os arquivos da secretaria.
g)  Secretariar a sessões da Diretoria e Assembléia Geral.

Art. 28º - Compete ao Tesoureiro:

a)  Arrecadar as contribuições dos associados, rendas, auxílios e
    donativos;
b)  Apresentar relatórios de receitas e despesas, sempre que solicitado;
c)  Apresentar ao Conselho Fiscal a escrituração da entidade, os
    relatórios de desempenho financeiro, contábil e sobre as operações
    patrimoniais realizadas;
d)  Supervisionar a contabilidade da entidade e conservar sob sua guarda
    e responsabilidade os documentos contábeis da entidade;
e)  Assinar convênios, contratos, documentos financeiros, movimentar
    contas bancárias e emitir cheques, sempre em conjunto com o
    Presidente.

§ Único -- Caso o Tesoureiro tiver qualquer tipo de impedimento para
efetuar as transações bancárias em nome da entidade, assumirá esta
responsabilidade o Secretário.

## CAPÍTULO IV - DOS RECURSOS FINANCEIROS

Art. 29º - Os recursos financeiros necessários à manutenção da
instituição poderão ser obtidos por:

a)  Termos de parcerias, convênios e contratos firmados com o Poder
    Público para financiamento de projetos na sua área de atuação;
b)  Contratos e acordos firmados com empresas e agências nacionais e
    internacionais;
c)  Doações legados e heranças;
d)  Rendimento de aplicações de seus ativos financeiros e outros,
    pertinentes ao patrimônio sob a sua administração;
e)  Contribuição dos Associados;
f)  Recebimentos de direitos autorais, direito de imagem e direito de
    uso de marcas registradas em nome do **ICTL**.

## CAPÍTULO V - DO PATRIMÔNIO

Art. 30º - O patrimônio do **ICTL** será constituído pelos bens imóveis,
móveis, registrados em seu nome ou por ela adquiridos, por bens a ela
destinada por pessoas físicas ou jurídica, doações, convênios e termo de
parceria que celebrar.

Art. 31º - Na hipótese da entidade obter e, posteriormente, perder a
qualificação instituída pela lei 9790/99, os acervos patrimoniais
disponíveis, adquiridos com recursos públicos durante o período em que
perdurou aquela qualificação, será transferido a outra pessoa jurídica
qualificada nos termos da mesma lei, preferencialmente com o mesmo
objetivo social.

Art. 32º - No caso de dissolução da entidade, o remanescente do seu
patrimônio líquido, será destinado à outra entidade de fins não
econômicos e com semelhante objetivo social.

## CAPÍTULO VI - DA PRESTAÇÃO DE CONTAS

Art. 33º - A prestação de contas do **ICTL** observará:

a)  Os princípios fundamentais de contabilidade e as normas brasileiras
    de contabilidade;
b)  A publicidade, por qualquer meio eficaz, no encerramento do
    exercício fiscal, ao relatório de atividades e das demonstrações
    financeiras da entidade, incluindo certidões negativas de débitos
    junto ao INSS e ao FGTS, colocando-os a disposição para o exame de
    qualquer cidadão;
c)  A realização de auditoria, inclusive por auditores externos
    independentes se for o caso, da aplicação dos eventuais recursos
    objeto de termo de parceria, conforme previsto em regulamento;
d)  A prestação de contas de todos os recursos e bens de origem pública
    recebidos será feita, conforme determina o parágrafo único do Art.
    70 da Constituição Federal.

## CAPÍTULO VII - DAS DISPOSIÇÕES GERAIS

Art. 34º - A entidade só poderá ser extinta ou dissolvida por
deliberação da Assembleia Geral, especialmente convocada para este fim,
mediante o voto favorável de 2/3 (dois terços) dos presentes desde que
observado o quorum de metade mais um dos associados, quando se tornar
impossível a continuação de suas atividades.

Art. 35º - Os casos omissos serão resolvidos pela Diretoria e
referendados pela Assembleia Geral.

Art. 36º - O presente Estatuto Social entra em vigor na data de sua
aprovação.

Curitiba, 29 de abril de 2021.

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\\
Antonio Soares de Azevedo Terceiro\\
Presidente da mesa e Presidente do ICTL

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\\
Daniel Lenharo de Souza\\
Secretário da mesa e Secretário do ICTL

Visto do Advogado:\\
\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\\
Victor Augusto Horochovec\\
OAB/PR 50.792
