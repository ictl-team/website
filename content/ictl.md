---
title: ICTL
---

# ICTL – Instituto para Conservação de Tecnologias Livres

## Quem somos

O ICTL – Instituto para Conservação de Tecnologias Livres – é uma associação sem fins lucrativos cujo objetivo é promover a divulgação, o desenvolvimento e a defesa do Software Livre. O ICTL desenvolve ações para o avanço do Software Livre em geral, e fornece uma estrutura institucional para projetos de Software Livre, de forma que eles possam receber doações, realizar despesas, e manter recursos.

## O que faz o ICTL

## ICTL e o Software Livre

# Contato com o ICTL


