class Nanoc::Core::CompilationItemView
  def slug
    File.basename(identifier, '.md')
  end
end
